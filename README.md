*[English version available here](README_en.md)*

# Utilisation de l’API Fullcar e-convoyage


**Chacune des requêtes décrites dans cette documentation devront être préfixées de l’URL https://e-convoyage.fr/api/v1 et devront contenir un paramètre `api_token` ou un header `Api-Token` correspondant à votre clé de connexion que vous pourrez trouver dans la partie « Récupération de la clé d’API ».**

------

*Les différents tests peuvent être effectués sur la version de développement d'e-convoyage. Le principe reste le même, cependant, toutes vos requêtes devront être préfixées de l'URL http://e-convoyage-test.avuer.be/api/v1.*

*Pour utiliser cette version de développement créez-vous simplement  un compte à l'adresse http://e-convoyage-test.avuer.be/authentification/inscription et demandez à un membre de Fullcar Services de l'activer.*

*Remarque : Aucun e-mail créé sur cette version ne sera envoyé au véritable destinataire.*

------

## Récupération de la clé d’API

Connectez-vous sur votre espace personnel https://e-convoyage.fr et rendez-vous dans la partie mes informations à l’adresse https://e-convoyage.fr/compte/info pour récupérer ou créer une clé d’API.

Cette clé d’API vous sera utile pour chacune des requêtes qui seront effectuées par la suite : ajoutez simplement un paramètre `api_token` ou un header `Api-Token` dans vos requêtes pour être authentifié.

-----

## Recherche de convoyage

#### Récupération d’une liste de convoyages  (`GET /convoyage`)

Vous avez la possibilité de récupérer les informations partielles d’une liste de convoyage.
Pour ce faire exécutez une requête GET avec si besoin des paramètres de recherche sur les champs :

* `reference` (string) : La référence du convoyage
* `reference_externe` (string) : Votre propre référence
* `id_societe` (int) : L'id de la société liée au convoyage
* `societe` (string) : Le nom de la société liée au convoyage
* `marque` (string) : La marque de la voiture
* `modele` (string) : Le modèle de la voiture
* `immatriculation` (string) : L'immatriculation de la voiture
* `debut_date` (date) : La date de départ du convoyage
* `fin_date` (date): La date d'arrivée du convoyage
* `statut` (string | tableau de string) : Un regroupement de statut du convoyage
    * `demande` : Les demandes effectuées pas encore confirmées, ce groupe contiendra les convoyages avec le statut :
        * `Commande envoyée` : La commande a été envoyée à Fullcar Services en est en attente de validation
    * `en_cours` : Les convoyages en cours, ce groupe contiendra les convoyages avec le statut :
        * `Confirmé` : La commande a été validée par Fullcar Services
        * `En cours` : Le convoyage va bien démarré, le convoyeur va bientôt enlever le véhicule
        * `Véhicule enlevé` : Le véhicule a été enlevé par le convoyeur
        * `Véhicule sur parc` : Le véhicule a été déposé sur un parc de stockage par le convoyeur (Seulement pour un convoyage mixte * Route - Camion)
        * `Véhicule sorti parc` : Le véhicule a été retiré sur un parc de stockage par le convoyeur (Seulement pour un convoyage mixte Route - Camion)
        * `Véhicule livré` : Le véhicule a été livré par le convoyeur
        * `Véhicule restitué` : Le véhicule a été restitué par le convoyeur (Similaire à "Véhicule livré" mais dans le cas d'une restitution)
        * `Incident` : Un incident empêchant la complétion du convoyage s'est produit
    * `fini` : Les convoyages terminés, ce groupe contiendra les convoyages avec le statut :
        * `Terminé` : Le convoyage est terminé
        * `En attente du loueur` : Le convoyage est terminé mais le véhicule n'a pas encore été récupéré par le loueur (Seulement pour un convoyage de type e-restitution)
        * `Restitué au loueur` : Le convoyage est terminé est véhicule a été récupéré par le loueur (Seulement pour un convoyage de type e-restitution)
    * `annule` : Les convoyages annulés ou refusés, ce groupe contiendra les convoyages avec le statut :
        * `Commande refusée` : La commande ne peut pas être traitée par Fullcar Services
        * `Annulé` : Le convoyage a été annulé par Fullcar Services
* `commentaire`: Le commentaire transmis à Fullcar Services

Les paramètres de recherche de date peuvent être suivi de complément

* `__noteq` : Une valeur différente
* `__gt` : Une valeur supérieure
* `__gte` : Une valeur supérieure ou égale
* `__lt` : Une valeur inférieure
* `__lte` : Une valeur inférieur ou égale

Les références, la marque, le modèle seront toujours des recherches "partielles" : si vous recherchez la référence 123, l'API pourrait par exemple vous retourner les convoyages "L-123", "L-1234", "R-21234", etc...

Deux autres paramètres peuvent être ajoutés

* `_page` (int) : Le numéro de la page a afficher (par défaut 1)
* `_per_page` (int) : Le nombre de résultat à retourner (par défaut 20, max 100)

Ainsi, une requête GET de type `/?marque=Renault&modele=Twi&debut_date__gte=2017-01-01` vous retournera l'ensemble des convoyages des voitures Renault avec un modèle contenant la chaine de caractère "Twi" et une date de départ supérieure ou égale au 1er janvier 2017.

L’objet JSON retourné est composé de deux sections distinctes :

* `data` : Un tableau d’objet contenant les informations basique des convoyages correspondant à la recherche
* `pagination` : les données de pagination avec le numéro de la page actuelle, le nombre total de résultat, le numéro du premier résultat retourné, le numéro du dernier résultat retourné

![](convoyages_list.png)

#### Récupération d’un convoyage en particulier (`GET /convoyage/[REFERENCE]`)

Chaque convoyage est identifié par une référence. Une simple requête GET de type `/L-XXXX` vous permettra de récupérer un objet JSON contenant les informations complètes du convoyage.

L’objet récupéré est composé de l’ensemble des informations du convoyage : la référence, les informations du véhicule,  les dates, les informations du contact, le prix de la prestation et de ses options, ...

Vous pouvez également retrouver une section avec la liste des échanges avec Fullcar Services, la liste des documents et les informations sur le statut de la facturation de ce convoyage.

![](convoyage_detail.png)

## Création d’un convoyage

La création d’un convoyage se fait en plusieurs étapes :

* Étape 1 : vous commencez par envoyer au serveur un objet contenant les informations de votre convoyage
* Étape 2 : le serveur vous renvoie alors les différentes possibilités de transport pour ce convoyage
* Étape 3 : vous validez le devis en renvoyant au serveur votre choix de transport

#### Étape 1 : Mise en place des informations principales (`POST /demande-devis`)

La première étape de création d’un convoyage consiste à envoyer une requête POST au serveur avec les informations suivantes :

* `reference_externe` : Votre propre référence
* `marque` : (obligatoire) La marque de la voiture
* `modele` : (obligatoire) Le modèle de la voiture
* `id_societe` : L'id de la société pour laquelle on crée le convoyage (voir Mes Sociétés). Par défaut correspondra à la société liée au compte
* `immatriculation` : (obligatoire) L'immatriculation de la voiture
* `debut_date` : (obligatoire si fin_date n'est pas saisi) La date de départ du convoyage
* `fin_date` : (obligatoire si debut_date n'est pas saisi) La date d'arrivée du convoyage (ne sera pas pris en compte si une debut_date est saisie)
* `heure_min` : L'heure minimale du convoyage (si debut_date est saisie : l'heure minimale du départ, sinon l'heure minimale d'arrivée)
* `heure_max` : L'heure maximale du convoyage (si debut_date est saisie : l'heure maximale du départ, sinon l'heure maximale d'arrivée) 
* `debut_adresse` : (obligatoire) Un objet contenant les informations de l’adresse de départ
    * `contact_civilite` : Madame|Monsieur
    * `contact_prenom` : Le prénom
    * `contact_nom` : (obligatoire) Le nom
    * `contact_email` : (obligatoire) L’email
    * `contact_tel` : (obligatoire) Le téléphone
    * `adresse_rue` : (obligatoire) La rue
    * `adresse_precision` : Le complément d’adresse
    * `adresse_cp` : (obligatoire) Le code postal
    * `adresse_ville` : (obligatoire) La ville
    * `adresse_code_pays` : Le code pays (deux lettres). Attention, e-convoyage n’autorise que les convoyages vers des pays frontaliers de la France dans une limite de 1400kms. Par défaut "FR".
* `fin_adresse` : (obligatoire) Un objet contenant les informations de l’adresse d’arrivée (voir debut_adresse)
* `commentaire`: Un éventuel commentaire à transmettre à Fullcar Services

![](convoyage_post.png)

#### Étape 2 : Analyse de la réponse du serveur

Une fois les informations principales du convoyage envoyées au serveur celui-ci vous renvoie un objet « devis » contenant les différents transports disponibles, leurs dates, leurs prix et les options disponibles :

* id : L’identifiant de la demande de devis
* solutions : Un tableau d’objet des solutions disponibles
    * `type_transport` : Le type de transport
    * `prix_transport` : Le prix HT du transport
    * `nb_km` : Le nombre de km roulés par le véhicule
    * `debut_date` : Si vous aviez saisi une date à l'étape précédente : cette date ; sinon un tableau contenant les dates de départ possible du convoyage
    * `fin_date` : Si vous aviez saisi une date à l'étape précédente : cette date ; sinon un tableau contenant les dates d'arrivée possible du convoyage
    * `options` : Un tableau d’objet contenant les options possibles pour ce transport
        * `id` : L’id de l’option qu’il faudra envoyer si elle est sélectionnée
        * `nom` : Le nom de l’option
        * `commentaire` : Un bref commentaire de l’option
        * `prix` : Le prix HT de l’option

![](convoyage_post_ret.png)

#### Étape 3 : Validation de la création (`PUT /demande-devis/[ID]`)

Une fois votre choix de transport effectué il vous suffit d'effectuer une requête PUT de type `demande-devis/[ID]` en y ajoutant :

* `type_transport` : Le type de transport choisi
* `debut_date` : Uniquement si vous aviez saisi `fin_date` à l'étape 1. Doit faire parti du tableau `debut_date` de l'étape 2
* `fin_date` : Uniquement si vous aviez saisi `debut_date` à l'étape 1. Doit faire parti du tableau `fin_date` de l'étape 2
* `options` : Un tableau des id des éventuelles options choisies.

Si le convoyage est correctement créé un objet convoyage vous sera renvoyé avec les détails complet du convoyage.

![](convoyage_put.png)

-----

## Création d'une restitution

Après avoir créé une livraison il vous est possible de demander une restitution de véhicule. 

Deux options sont possible : 
- la restitution libre : le véhicule effectue le trajet inverse de la livraison
- la restitution stockage : le véhicule est transporté vers le parc de stockage le plus proche 

La restitution est effectué dès que le premier véhicule a été livré.

#### Création d'une restitution libre (`POST /convoyage/[LIVRAISON_REFERENCE]/restitution-libre`)
 
#### Création d'une restitution stockage (`POST /convoyage/[LIVRAISON_REFERENCE]/restitution-stockage`)

Dans les deux cas seules trois informations sont nécessaire :

* `marque` : (obligatoire) La marque de la voiture
* `modele` : (obligatoire) Le modèle de la voiture
* `immatriculation` : (obligatoire) L'immatriculation de la voiture

Une fois ces informations postées, le serveur renvoie un objet « devis » et le cycle de création classique est utilisé, voir `Création d’un convoyage > Étape 2`.

-----

## Envoi de document

#### Envoi d'un nouveau document lié à un convoyage (`POST /convoyage/[REFERENCE]/document`)

Afin d'enregistrer un document pour un convoyage, une requête POST doît être effectuée sur  `/convoyage/[REFERENCE]/document`

Pour cela, deux parameters sont disponible :

* `fichier`: le document à envoyer
* `id_type_document`: (optionnel) le type de document
    * `PHO` : Photo
    * `PVL` : Procès verbal de livraison
    * `PV` : Procès verbal de restitution
    * `CG` : Carte grise
    * `DOC` : Autre document (par défaut)

-----

## Envoi de message à Fullcar Services

#### Envoi d'un nouveau message lié à un convoyage (`POST /convoyage/[REFERENCE]/message`)

Afin d'envoyer un nouveau message à Fullcar Services pour un convoyage, une requête POST doît être effectuée sur  `/convoyage/[REFERENCE]/message`

Pour cela, un unique parametre est requis :
* `message`: le message à envoyer

-----

## Annulation de convoyage

#### Demande d'annulation d'un convoyage (`POST /convoyage/[REFERENCE]/annuler`)

Afin d'envoyer une demande d'annulation à Fullcar Services pour un convoyage, une requête POST doît être effectuée sur  `/convoyage/[REFERENCE]/annuler`

Aucun paramètre n'est requis.

Attention, il ne s'agit que d'une demande d'annulation, Fullcar Services peut refuser cette demande. En fonction du statut et de la date prévue du convoyage celui-ci pourrait vous être facturé.

-----

## Mes factures

#### Récupération des factures liées à votre compte (`GET /facture`)

Vous pouvez récupérer à tout moment l'ensemble des factures émise pour vos convoyages.

Pour cela, effectuez une requête GET sur `/facture`. 

Des paramètres de recherche peuvent être ajoutés :
* `reference` (string) : La référence de la facture
* `reference_externe` (string) : Votre référence sur l'un des convoyages lié
* `immatriculation` (string) : L'immatriculation pour l'un des convoyages lié
* `date` (date) : La date d'emmission de la facture

La date peut être suivi de complément

* `__noteq` : Une valeur différente
* `__gt` : Une valeur supérieure
* `__gte` : Une valeur supérieure ou égale
* `__lt` : Une valeur inférieure
* `__lte` : Une valeur inférieur ou égale

Deux autres paramètres peuvent être ajoutés pour gérer la pagination :

* `_page` (int) : Le numéro de la page a afficher (par défaut 1)
* `_per_page` (int) : Le nombre de résultat à retourner (par défaut 20, max 100)

L’objet JSON retourné est composé de deux sections distinctes :

* `data` : Un tableau d’objet contenant les informations des factures correspondant à la recherche et les informations basiques des convoyages liés
* `pagination` : les données de pagination avec le numéro de la page actuelle, le nombre total de résultat, le numéro du premier résultat retourné, le numéro du dernier résultat retourné

![](facture.png)

-----

## Mes sociétés

#### Récupération des sociétés liées à votre compte (`GET /societes`)

Votre compte peut être lié à plusieurs sociétés. Pour récupérer les ids qui vous permettront par la suite de créer des convoyages il vous suffit d'effectuer une requête GET sur `/societes`.

Un tableau contenant l'ensemble de vos sociétés pour sera alors retourné.

![](societes.png)
