*[Version Française disponible ici](./README.md)*

# Using Fullcar e-convoyage API

**Each request described in this documentation must be prefixed with the URL https://e-convoyage.fr/api/v1 and must contain a `api_token` parameter or a `Api-Toekn` header corresponding to your connection key transmitted by Fullcar Services.**

------

*The various tests can be performed on the development version of IntraFB. The principle remains the same, however, all your requests must be prefixed with the URL http://e-convoyage-test.avuer.be/api/v1.*

*To be able to use this version (dev), you will need to create an account on this webpage  http://e-convoyage-test.avuer.be/authentification/inscription and ask a member at Fullcar Services to activate the account.*

*Note: No e-mail created on this version will be sent to the real recipient.*

------

## Obtaining your API key

Log in to your account https://e-convoyage.fr, and go to the details section here: https://e-convoyage.fr/compte/info to get your API key.

This key will allow you to perform any request on the API: simply add an `api_token` parameter or a `Api-Token` header to authenticate your requests.

-----

## Search for conveyance operation

#### Retrieving a conveyance list  (`GET /convoyage`)

You have the option of retrieving partial information from a conveyance list. To do this, execute a GET request with, if necessary, search parameters on the fields:

* `reference` (string) : The conveyor reference
* `reference_externe` (string) : Your customized reference
* `id_societe` (int) : The company id’s number
* `societe` (string) : The customer's company
* `marque` (string) : The brand of the vehicle
* `modele` (string) : The vehicle model
* `immatriculation` (string) : Registration
* `debut_date` (date) : The minimum date of the conveyance
* `fin_date` (date): The maximum date of the conveyance
* `statut` (string | array of strings) : The name of the action performed
    * `demande` : Order sent but not yet confirmed, this group will list the conveyances with the following status:
        * `Commande envoyée` : Order sent to Fullcar Services and pending for validation
    * `en_cours` : Conveyances in progress, this group will contain the conveyances with the status:
        * `Confirmé` : The order has been by Fullcar Services
        * `En cours` : The conveyance will begin shortly, the driver will soon pick up the vehicle
        * `Véhicule enlevé` : The vehicle has been picked up by the driver
        * `Véhicule sur parc` : The vehicle has been delivered on a compound by the driver (Only for multimodal * Road - Truck)
        * `Véhicule sorti parc` : The vehicle has been retrieved from the compound by the driver (Only for multimodal * Road - Truck)
        * `Véhicule livré` : Vehicle delivered by the driver
        * `Véhicule restitué` : The vehicle has been returned by the driver (Similar to « Vehicle delivered » but in a returned scenario)
        * `Incident` : An incident preventing the completion of the conveyance has occurred
    * `fini` : The conveyances completed, this group will contain the conveyances with the status:
        * `Terminé` : Conveyance completed
        * `En attente du loueur` : The conveyance is completed but has not been picked up by the leaser (Only for a conveyance type e-restitution)
        * `Restitué au loueur` : The conveyance is completed and the vehicle has been picked up by the leaser (Only for a conveyance type e-restitution)
    * `annule` : Canceled or refused conveyances this group will contain the conveyances with the status:
        * `Commande refusée` : The order cannot be processed by Fullcar Services
        * `Annulé` : The conveyance has been canceled by Fullcar Services
* `commentaire`: The comment sent to Fullcar Services

Date search parameters can be followed by add-in

* `__noteq` : A different value
* `__gt` : A higher value
* `__gte` : A value greater than or equal
* `__lt` : A lower value
* `__lte` : A lower value or equal

The referrences, the brand, the model will always be « partial » searches : If you search for the reference 123, the API could return you the conveyance "L-123", "L-1234", "R-21234", etc...

Two more parameters can be added

* `_page` (int) : The number of the page to display (by default 1)
* `_per_page` (int) : The number of results to return (by default 20, max 100)

So, a query GET de type `/?marque=Renault&modele=Twi&debut_date__gte=2017-01-01` will return all the Renault cars with a model containing the character string « Twi » and a departure date greater than or equal to January 1, 2017.

The return JSON object is composed of two distinct sections :

* `data` : An object table containing the basic information of the conveyances corresponding to the search
* `pagination` : the pagination data with the number of the current page, the total number of results, the number of the first result returned, the number of the last result returned

![](convoyages_list.png)

#### Recovery of a particular conveyance (`GET /convoyage/[REFERENCE]`)

Each conveyance is identified by a reference. A simple GET request of type `/L-XXXX` will allow you to retrieve a JSON object containing the complete information of the conveyance.

The recovered object is made up of all the information from the conveyance: its reference, the vehicle informations, the dates, the contacts informations, the price of the conveyance and its options, ...

You can also fin a section with the list of messages sent to/by Fullcar Services, the list of documents and informations about the billing status.

![](convoyage_detail.png)

## Creation of a conveyance

The creation of a conveyance is done in several steps :

* Step 1: you start by sending an object containing the information of your conveyance to the server. 
* Step 2: the server then sends you back the different transport options for this conveyor.
* Step 3: you validate the quote by sending your choice of transport back to the server.

#### Step 1: Establishment of main information (`POST /demande-devis`)

The first step in creating a conveyor belt is to send a POST request to the server with the following information:

* `reference_externe` : The reference given by the customer when ordering
* `marque` : (required) The brand of the vehicle
* `modele` : (required) The vehicle model
* `id_societe` : The company id’s number for which the conveyance has been created (see My companies). Default value will be the main company linked to the account.
* `immatriculation` : (required) Registration number
* `debut_date` : (required if fin_date is not set) The departure date of the conveyance
* `fin_date` : (required if debut_date is not set) The arrival date of the conveyance (will not be taken into account if a departure date is selected)
* `heure_min` : Minimal hour of the convoyance (if debut_date is set : minimal hour for the departure, otherwise minimal hour for the arrival)
* `heure_max` : Maximal hour of the convoyance (if debut_date is set : maximal hour for the departure, otherwise maximal hour for the arrival)
* `debut_adresse` : (required) An object containing the information of the departure address
    * `contact_civilite` : Madame|Monsieur
    * `contact_prenom` : First name
    * `contact_nom` : (required) Last name
    * `contact_email` : (required) Email contact
    * `contact_tel` : (required) Phone contact
    * `adresse_rue` : (required) Road
    * `adresse_precision` : Additional address
    * `adresse_cp` : (required) Zip code
    * `adresse_ville` : (required) City
    * `adresse_code_pays` : The country code (two letters). Please note, e-convoyage only authorizes conveyance to countries bordering France within a limit of 1400kms. Default value is "FR".
* `fin_adresse` : (required) An object containing the information of arrival address (see start_address)
* `commentaire`: Any comment to send to Fullcar Services

![](convoyage_post.png)

#### Step 2: Analysis of the server response

Once the main information of the conveyance has been sent to the server, it sends you a "devis" object containing the different transports available, their dates, their prices and the available options:

* id : The id of the quote request
* solutions : An object table of the available solutions
    * `type_transport` : The type of transport
    * `prix_transport` : The price excluding VAT of the transport
    * `nb_km` : The number of km driven by the vehicle
    * `debut_date` : If you had entered a date in the previous step: this date; otherwise an array containing the possible departure dates of the conveyance
    * `fin_date` : If you had entered a date in the previous step: this date; otherwise an array containing the possible arrival dates of the conveyance
    * `options` : An array of objects containing the possible options for this transport
        * `id` : The ID of the option to send if it is selected
        * `nom` : The option name
        * `commentaire` : A brief comment of the option
        * `prix` : The price excluding VAT of the option

![](convoyage_post_ret.png)

#### Step 3: Creation Validation (`PUT /demande-devis/[ID]`)

Once your choice of transport has been made, all you have to do is make a PUT request of type`demande-devis/[ID]` by adding:

* `type_transport` : The type of transport chosen
* `debut_date` : Only if you had entered `fin_date` at step 1. Must be part of the `debut_date` array at step 2
* `fin_date` : Only if you had entered `debut_date` at step 1. Must be part of the `fin_date` array at step 2
* `options` : An array of ids of any options chosen.

If the conveyance is correctly created, a conveyance object will be returned to you with the complete details of the conveyance.

![](convoyage_put.png)

-----

## Creation of a restitution

After the creation of the delivery you can ask for the restitution of a vehicle.

Two options are available : 
- free e-restitution : the vehicle takes the opposite route of the delivery
- e-restitution storage : the vehicle is conveyed to the nearest storage plant 

The restitution is done as soon as the first vehicle has been delivered.

#### Creation of a free e-restitution (`POST /convoyage/[DELIVERY_REFERENCE]/restitution-libre`)
 
#### Création of a e-restitution storage (`POST /convoyage/[DELIVERY_REFERENCE]/restitution-stockage`)

In both cases only three informations are needed :

* `marque` (required) : The brand of the vehicle
* `modele` (required) : The vehicle model
* `immatriculation` (required) : Registration

Once these informations have been posted, the server returns a « devis » object and the usual creation cycle is used, see `Creation of a conveyance > Step 2`.

-----

## Sending a document

#### How to send a new document linked to a conveyance (`POST /convoyage/[REFERENCE]/document`)

To save a document for a conveyance, a POST request must be done on `/convoyage/[REFERENCE]/document`

For this, two parameters are used :

* `fichier`: the file you want to send
* `id_type_document`: (optional) the type of document you want to send
    * `PHO` : Photo
    * `PVL` : Report of delivery
    * `PV` : Restitution report
    * `CG` : Car registration document
    * `DOC` : Other document (default)

-----

## Sending a message to Fullcar Services

#### How to send a new message linked to a conveyance (`POST /convoyage/[REFERENCE]/message`)

To send a new message to Fullcar Services for a conveyand, a POST request must be done on `/convoyage/[REFERENCE]/message`

For this, a single parameter is required :
* `message`: the message to send

-----

## Cancelling a conveyance

#### How to send a cancel request (`POST /convoyage/[REFERENCE]/annuler`)

In ordre do send a cancel request to Fullcar Services, a POST request must be done on  `/convoyage/[REFERENCE]/annuler`

No additional parameter is required.

However, this request may be refused by Fullcar Services. Based on the status and the date, the conveyand may be billed.

-----

## My bills

#### How to retrieve bills linked to my account (`GET /facture`)

At any time you can retrieve all bills emitted for your conveyances.

To do so, a GET request must be done on `/facture`. 

Search parameters can be added :
* `reference` (string) : The bill reference
* `reference_externe` (string) : Your own reference on one of the linked conveyance
* `immatriculation` (string) : The registration on one of the linked conveyance 
* `date` (date) : The date when the bill was created

The date can be followed by add-in

* `__noteq` : A different value
* `__gt` : A higher value
* `__gte` : A value greater than or equal
* `__lt` : A lower value
* `__lte` : A lower value or equal

Two more parameters can be added

* `_page` (int) : The number of the page to display (by default 1)
* `_per_page` (int) : The number of results to return (by default 20, max 100)

The returned JSON object is composed of two distinct sections :

* `data` : An object table containing the basic information of the bills corresponding to the search and their linked convoyances
* `pagination` : The pagination data with the number of the current page, the total number of results, the number of the first result returned, the number of the last result returned

![](facture.png)
-----

## My companies

#### Recovery of companies linked to your account (`GET /societes`)

Your account can be linked to several companies. To retrieve the ids which will then allow you to create convoys, you just need to perform a GET request on `/societes`.

An array containing all of your companies will then be returned.

![](societes.png)

